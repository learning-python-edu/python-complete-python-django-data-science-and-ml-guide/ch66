# Chapter 66: Create a Django project

## Create Django project

In newly created empty Python project execute following command:
```shell
django-admin startproject base .
```

This command will create an empty Django project.

## Start and verify the Django server

To start Django server execute following command:
```shell
python manage.py runserver
```

To verify Django server open [http://127.0.0.1:8000/](http://127.0.0.1:8000/) in the browser.
This will show default page.

To access admin page open following URL: [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin). 
This will show an admin login page, but for now it will not be possible because there is no admin user created yet.
